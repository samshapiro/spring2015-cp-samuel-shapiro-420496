<!DOCTYPE html>
<head>
<title>Sam Shapiro</title>
<meta charset="utf-8"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Playfair+Display%7CJosefin+Sans%7CAnton%7CCrete+Round%7CRoboto+Condensed%7CIndie+Flower%7CFrancois+One%7CPacifico%7CAlegreya%7CPathway+Gothic+One%7CAmatic+SC%7CArchitects+Daughter%7CKaushan+Script%7CCovered+By+Your+Grace%7CLobster+Two' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div id="nav">
<h3>Site Navigation</h3>
<ul>
<li><a href="index.php">Home</a></li>
<li><a href="commercials.php">Commercials</a></li>
<li><a href="photography.php">Photography</a></li>
</ul>
</div>
<div id="main"> 
<h1>Sam Shapiro</h1>

<?php
    $mysqli = new mysqli("localhost", "samshapirosite", "rover", "samshapirositecontent");
    // Check connection
    if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
    }
    $stmt = $mysqli->prepare("SELECT fileloc FROM content where id=7;");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->execute();
    $stmt->bind_result($fileloc);
    echo "\n";
    while($stmt->fetch()){
	printf("<img src=%s width='700' height='400' alt='img'>\n",
		htmlspecialchars($fileloc)
	);
    }
    echo "\n";
    $stmt->close();
?>

</div>
<script>
$(document).ready(function() {
	// Check for hash value in URL
    var hash = window.location.hash.substr(1);
    var href = $('#nav li a').each(function(){
        var href = $(this).attr('href');
        if(hash==href.substr(0,href.length-4)){
            var toLoad = hash+'.php #main';
            $('#main').load(toLoad)
        }
    });
     
    $('#nav li a').click(function(){
     
    var toLoad = $(this).attr('href')+' #main';
    $('#main').hide('fast',loadContent);
    $('#load').remove();
    $('#wrapper').append('<span id="load">LOADING...</span>');
    $('#load').fadeIn('normal');
    window.location.hash = $(this).attr('href').substr(0,$(this).attr('href').length-4);
    function loadContent() {
        $('#main').load(toLoad,'',showNewContent())
    }
    function showNewContent() {
        $('#main').show('normal',hideLoader());
    }
    function hideLoader() {
        $('#load').fadeOut('normal');
    }
    return false;
     
    });
});
</script>
</body>
</html>